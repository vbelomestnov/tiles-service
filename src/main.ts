import * as moduleAlias from 'module-alias';
import { resolve } from 'path';
moduleAlias.addAlias('@loaders.gl/3d-tiles', resolve('../loaders.gl/modules/3d-tiles'));
moduleAlias.addAlias('@loaders.gl/arrow', resolve('../loaders.gl/modules/arrow'));
moduleAlias.addAlias('@loaders.gl/basis', resolve('../loaders.gl/modules/basis'));
moduleAlias.addAlias('@loaders.gl/core', resolve('../loaders.gl/modules/core'));
moduleAlias.addAlias('@loaders.gl/csv', resolve('../loaders.gl/modules/csv'));
moduleAlias.addAlias('@loaders.gl/draco', resolve('../loaders.gl/modules/draco'));
moduleAlias.addAlias('@loaders.gl/gis', resolve('../loaders.gl/modules/gis'));
moduleAlias.addAlias('@loaders.gl/gltf', resolve('../loaders.gl/modules/gltf'));
moduleAlias.addAlias('@loaders.gl/i3s', resolve('../loaders.gl/modules/i3s'));
moduleAlias.addAlias('@loaders.gl/images', resolve('../loaders.gl/modules/images'));
moduleAlias.addAlias('@loaders.gl/json', resolve('../loaders.gl/modules/json'));
moduleAlias.addAlias('@loaders.gl/kml', resolve('../loaders.gl/modules/kml'));
moduleAlias.addAlias('@loaders.gl/las', resolve('../loaders.gl/modules/las'));
moduleAlias.addAlias('@loaders.gl/mvt', resolve('../loaders.gl/modules/mvt'));
moduleAlias.addAlias('@loaders.gl/obj', resolve('../loaders.gl/modules/obj'));
moduleAlias.addAlias('@loaders.gl/pcd', resolve('../loaders.gl/modules/pcd'));
moduleAlias.addAlias('@loaders.gl/ply', resolve('../loaders.gl/modules/ply'));
moduleAlias.addAlias('@loaders.gl/potree', resolve('../loaders.gl/modules/potree'));
moduleAlias.addAlias('@loaders.gl/tables', resolve('../loaders.gl/modules/tables'));
moduleAlias.addAlias('@loaders.gl/terrain', resolve('../loaders.gl/modules/terrain'));
moduleAlias.addAlias('@loaders.gl/tiles', resolve('../loaders.gl/modules/tiles'));
moduleAlias.addAlias('@loaders.gl/video', resolve('../loaders.gl/modules/video'));
moduleAlias.addAlias('@loaders.gl/wkt', resolve('../loaders.gl/modules/wkt'));
moduleAlias.addAlias('@loaders.gl/polyfills', resolve('../loaders.gl/modules/polyfills'));
moduleAlias.addAlias('@loaders.gl/loader-utils', resolve('../loaders.gl/modules/loader-utils'));

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

import '@loaders.gl/polyfills';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: ['error', 'warn', 'log', 'verbose', 'debug'],
  });
  app.enableCors();
  await app.listen(parseInt(process.env.PORT, 10) || 80,);
}
bootstrap();
