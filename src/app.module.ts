import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { I3sModule } from './i3s/i3s.module';
import { ThreeDTilesModule } from './3d-tiles/3d-tiles.module';

@Module({
  imports: [I3sModule, ThreeDTilesModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
