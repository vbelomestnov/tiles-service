import { Test, TestingModule } from '@nestjs/testing';
import { SceneServerController } from './scene-server.controller';
import { StubLayerService } from './stub-layer/stub-layer.service';

describe('SceneServer Controller', () => {
  let controller: SceneServerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SceneServerController],
      providers: [StubLayerService]
    }).compile();

    controller = module.get<SceneServerController>(SceneServerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
