import { Controller, Get, Res, Req } from '@nestjs/common';
import { StubLayerService } from './stub-layer/stub-layer.service';
import { Response, Request } from 'express';

@Controller('i3s')
export class SceneServerController {
    constructor(private readonly layerService: StubLayerService) {}
    @Get('*')
    async getServerInfo(@Req() req: Request, @Res() res: Response): Promise<void> {
        const fileName = await this.layerService.getFileNameByUrl(req.path);
        res.sendFile(fileName);
    }
}
