import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { join } from 'path';
import { promises } from 'fs';

const STUBS_PATH = join(process.cwd(), './stubs');

@Injectable()
export class StubLayerService {
    async getFileNameByUrl(url: string): Promise<string> {
        const extensions: string[] = ['json', 'bin', 'jpg', 'bin.dds'];
        for (const ext of extensions) {
            const fileName = `${STUBS_PATH}${url}/index.${ext}`;
            try {
                await promises.access(fileName);
                return fileName;
            } catch {
                continue;
            }
        }
        throw new HttpException({
            status: HttpStatus.NOT_FOUND,
            error: 'File not found',
        }, HttpStatus.FORBIDDEN);
    }
}
