import { Test, TestingModule } from '@nestjs/testing';
import { StubLayerService } from './stub-layer.service';

describe('StubLayerService', () => {
  let service: StubLayerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StubLayerService],
    }).compile();

    service = module.get<StubLayerService>(StubLayerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
