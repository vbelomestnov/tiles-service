import { Module } from '@nestjs/common';
import { SceneServerController } from './scene-server/scene-server.controller';
import { StubLayerService } from './scene-server/stub-layer/stub-layer.service';

@Module({
imports: [],
  controllers: [SceneServerController],
  providers: [StubLayerService],
})
export class I3sModule {}
