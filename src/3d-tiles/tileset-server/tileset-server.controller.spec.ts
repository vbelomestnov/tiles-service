import { Test, TestingModule } from '@nestjs/testing';
import { TilesetServerController } from './tileset-server.controller';
import { StubTilesetService } from './stub-tileset/stub-tileset.service';

describe('TilesetServer Controller', () => {
  let controller: TilesetServerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TilesetServerController],
      providers: [StubTilesetService]
    }).compile();

    controller = module.get<TilesetServerController>(TilesetServerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
