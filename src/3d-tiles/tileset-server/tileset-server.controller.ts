import { Controller, Get, Res, Req } from '@nestjs/common';
import { Response, Request } from 'express';
import { StubTilesetService } from './stub-tileset/stub-tileset.service';

@Controller('3d-tiles')
export class TilesetServerController {
    constructor(private readonly tilesetService: StubTilesetService) {}
    @Get('*')
    async getServerInfo(@Req() req: Request, @Res() res: Response): Promise<void> {
        const fileName = await this.tilesetService.getFileNameByUrl(req.path);
        res.sendFile(fileName);
    }
}
