import { Test, TestingModule } from '@nestjs/testing';
import { StubTilesetService } from './stub-tileset.service';

describe('StubTilesetService', () => {
  let service: StubTilesetService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StubTilesetService],
    }).compile();

    service = module.get<StubTilesetService>(StubTilesetService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
