import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { join } from 'path';
import { promises } from 'fs';
import { load } from '@loaders.gl/core';
import { CesiumIonLoader } from '@loaders.gl/3d-tiles';
import { DracoLoader } from '@loaders.gl/draco';
import { registerLoaders } from '@loaders.gl/core';
import { Tileset3D } from '@loaders.gl/tiles';

const STUBS_PATH = join(process.cwd(), './stubs');
const ION_TOKEN = process.env.IonToken || 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI4ZTZlMjI3MC1lYmI0LTRmMjctODU0Mi0yN2ZiNmYxY2VhODkiLCJpZCI6MjYxMzMsInNjb3BlcyI6WyJhc2wiLCJhc3IiLCJhc3ciLCJnYyIsInByIl0sImlhdCI6MTU4NzMzMjQ4Nn0.IeeAR9E0BXo3hMN7JhisCrE8qES7Sx_fuvmD4UXoLJo'; // eslint-disable-line

@Injectable()
export class StubTilesetService {
    async getFileNameByUrl(url: string): Promise<string> {
        const fileName = `${STUBS_PATH}${url}`;
        try {
            await promises.access(fileName);
            return fileName;
        } catch { }
        throw new HttpException({
            status: HttpStatus.NOT_FOUND,
            error: 'File not found',
        }, HttpStatus.FORBIDDEN);
    }

    async loadTile(): Promise<any> {
        registerLoaders([DracoLoader]);
        const tilesetJson = await load(
            // Remember this tileset which was loaded for demo
            // 'https://raw.githubusercontent.com/visgl/deck.gl-data/master/3d-tiles/RoyalExhibitionBuilding/tileset.json',
            `${STUBS_PATH}/3d-tiles-with-children/tileset.json`,
            CesiumIonLoader
        );
        const hrstart = process.hrtime();
        console.log('Execution start');
        const tileset3d = new Tileset3D(tilesetJson);
        await tileset3d.loadAllTiles();
        console.log('DONE!!!!');
        const hrend = process.hrtime(hrstart)
        console.info('Execution time (hr): %ds %dms', hrend[0], hrend[1] / 1000000)
        return this.generateTilesetData(tileset3d);
    }

    generateTilesetData(tileset3d: Tileset3D) {
        const tilesetData = this.analyseTile(tileset3d.root);
        tilesetData.name = tileset3d.basePath;
        return tilesetData;
    }

    analyseTile(tile) {
        const type = tile.type;
        let size = 'N/A';
        let pointCount = 'N/A';
        if (type === 'pointcloud' && tile.content) {
            size = tile.content.byteLength;
            pointCount = tile.content.pointCount;
        }
        if (type === 'scenegraph') {
            size = tile.content.byteLength;
        }
        const data = {
            name: tile.id,
            type,
            size,
            pointCount,
            children: [],
        };
        for (const child of tile.children) {
            data.children.push(this.analyseTile(child));
        }
        return data;
    }
}
