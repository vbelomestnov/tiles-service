import { Module } from '@nestjs/common';
import { TilesetServerController } from './tileset-server/tileset-server.controller';
import { StubTilesetService } from './tileset-server/stub-tileset/stub-tileset.service';
import { TilesetParseController } from './tileset-parse/tileset-parse.controller';

@Module({
imports: [],
  controllers: [TilesetServerController, TilesetParseController],
  providers: [StubTilesetService],
})
export class ThreeDTilesModule {}
