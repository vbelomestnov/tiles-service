import { Test, TestingModule } from '@nestjs/testing';
import { TilesetParseController } from './tileset-parse.controller';

describe('TilesetParse Controller', () => {
  let controller: TilesetParseController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TilesetParseController],
    }).compile();

    controller = module.get<TilesetParseController>(TilesetParseController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
