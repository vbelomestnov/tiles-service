import { Controller, Get } from '@nestjs/common';
import { StubTilesetService } from '../tileset-server/stub-tileset/stub-tileset.service';

@Controller('tileset-parse')
export class TilesetParseController {
    constructor(private readonly tilesetService: StubTilesetService) {}
    @Get()
    async getTileData(): Promise<any> {
        return this.tilesetService.loadTile();
    }
}
